{$ifndef ALLPACKAGES}
{$mode objfpc}{$H+}
program fpmake;

uses fpmkunit;

Var
  T : TTarget;
  P : TPackage;
begin
  With Installer do
    begin
{$endif ALLPACKAGES}

    P:=AddPackage('cnocThreadingTools');

    P.Version:='0.5.2';
    P.Author := 'Joost van der Sluis / CNOC';
    P.License := 'LGPL with modification, ';
    P.HomepageURL := 'www.lazarussupport.com';
    P.Email := 'joost@cnoc.nl';
    P.Description := 'Thread-safe queue and monitor';
    P.OSes:=AllOSes;

    P.Dependencies.Add('fcl-base');
 
    T:=P.Targets.AddUnit('cnocqueue.pp');
{$ifndef ALLPACKAGES}
    Run;
    end;
end.
{$endif ALLPACKAGES}

